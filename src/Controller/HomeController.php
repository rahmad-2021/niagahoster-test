<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
  /**
   * @Route("/", name="home_controllerc")
   */
    public function index(): Response
    {
        $data = [
          'headers' => [
            [
              'text'=>'0274-5305505',
              'class'=>'fas fa-phone-alt mr-2',
              'style'=>''
            ],
            [
              'text'=>'Live Chat',
              'class'=>'fas fa-comment-alt mr-2',
              'style'=>''
            ],
            [
              'text'=>'Member Area',
              'class'=>'fas fa-user-circle mr-2 mt-n1',
              'style'=>'font-size: 175%;'
            ]
          ],
          'menus'=> [
            'Hosting',
            'Domain',
            'Server',
            'Website',
            'Afiliasi',
            'Promo',
            'Pembayaran',
            'Review',
            'Kontak',
            'Blog'
          ],
          'mini_menus'=>[
            [
              'text'=>'Call',
              'icon'=>'fas fa-phone-alt mr-1'
            ],
            [
              'text'=>'Chat',
              'icon'=>'fas fa-comment-alt mr-1'
            ],
            [
              'text'=>'Member',
              'icon'=>'fas fa-user-circle mr-1 fs-125'
            ]
          ],
          'packages'=>[
            'bayi'=>[
              'name'=>'Bayi',
              'old_price'=>'19.900',
              'new_price'=>'14.900',
              'users'=>'938',
            ],
            'pelajar'=>[
              'name'=>'Pelajar',
              'old_price'=>'46.900',
              'new_price'=>'23.450',
              'users'=>'4.168',
            ],
            'personal'=>[
              'name'=>'Personal',
              'old_price'=>'58.900',
              'new_price'=>'38.900',
              'users'=>'10.017',
            ],
            'bisnis'=>[
              'name'=>'Bisnis',
              'old_price'=>'109.900',
              'new_price'=>'65.900',
              'users'=>'3.552',
            ]
          ]
        ];
        return $this->render('home/index.html.twig', $data);
    }
}
